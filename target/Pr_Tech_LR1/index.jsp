<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html xmlns:th = "http://www.thymeleaf.com" lang="en">
<head>
    <meta charset="UTF-8">
    <title th:text = "'Меню'"></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/css/bootstrap.min.css" integrity="sha384-DhY6onE6f3zzKbjUPRc2hOzGAdEf4/Dz+WJwBvEYL/lkkIsI3ihufq9hk9K4lVoK" crossorigin="anonymous">

</head>
<body>
<div class="container">
    <h3>Меню</h3>
    <hr>
    <form method="get" action="<c:url value='/car/list'/>">
        <input type="submit" value="Список машин"/>
    </form>
    <br/>
    <form method="get" action="<c:url value='/color/list'/>">
    <input type="submit" value="Список цветов"/>
    </form>
    <br/>
    <form method="get" action="<c:url value='/chip/list'/>">
    <input type="submit" value="Список чипов"/>
    </form>
    <br/>
    <form method="get" action="<c:url value='/engine/list'/>">
    <input type="submit" value="Список двигателей"/>
    </form>
    <br/>
</div>
</body>
</html>


