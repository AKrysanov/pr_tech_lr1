  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="ru" xmlns:th="http://www.thymeleaf.org">

<head>
  <meta charset="UTF-8">

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/css/bootstrap.min.css" integrity="sha384-DhY6onE6f3zzKbjUPRc2hOzGAdEf4/Dz+WJwBvEYL/lkkIsI3ihufq9hk9K4lVoK" crossorigin="anonymous">

  <title>Список цветов</title>
</head>
<body>
<div class="container">

  <h3>Цвета</h3>
  <hr>
    <form method="post" action="<c:url value='/color/add'/>">
    <input type="submit" name="update" value="Добавить цвет"/>
    </form>
  <br/>


  <hr>

  <table class="table table-bordered table-striped">
    <thead class="table-dark">
    <tr>
      <th>ID</th>
      <th>Цвета</th>
      <th>Параметры</th>

    </tr>
    </thead>

    <tbody>
    <c:forEach var="tempColor" items="${color}">
      <tr>
      <td><c:out value="${tempColor.id}"/></td>
      <td><c:out value="${tempColor.name}"/></td>


      <td>
      <form method="post" action="<c:url value='/color/update'/>">
          <input type="number" hidden name="id" value="${tempColor.id}" />
          <input type="submit" name="update" value="Обновить"/>
      </form>
      <form method="post" action="<c:url value='/color/delete'/>">
          <input type="number" hidden name="id" value="${tempColor.id}" />
          <input type="submit" name="delete" value="Удалить"/>
      </form>
      </td>

    </tr>
    </c:forEach>

    </tbody>
  </table>

  <hr>
    <form method="get" action="<c:url value='/'/>">
    <input type="submit" value="В главное меню"/>
    </form>


</div>
</body>
</html>