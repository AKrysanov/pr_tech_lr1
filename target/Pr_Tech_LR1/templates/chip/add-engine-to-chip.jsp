<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="ru" xmlns:th="http://www.thymeleaf.org">

<head>
  <meta charset="UTF-8">

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/css/bootstrap.min.css" integrity="sha384-DhY6onE6f3zzKbjUPRc2hOzGAdEf4/Dz+WJwBvEYL/lkkIsI3ihufq9hk9K4lVoK" crossorigin="anonymous">

  <title>Добавление двигателей</title>
</head>
<body>
<div class="container">

  <h3> <c:out value="${chip.name}"/></h3>
  <hr>
  <table class="table table-bordered table-striped">
    <thead class="table-dark">
    <tr>
      <th>ID</th>
      <th>HP</th>
      <th>VOL</th>
    </tr>
    </thead>

    <tbody>
    <c:forEach var="tempEngine" items="${engine}">
    <tr>
      <td><c:out value="${tempEngine.id}"/></td>
      <td><c:out value="${tempEngine.hp}"/></td>
      <td><c:out value="${tempEngine.vol}"/></td>
      <td>
        <form method="post" action="<c:url value='/chip/addengine'/>">
          <input type="number" hidden name="id_en" value="${tempEngine.id}" />
          <input type="number" hidden name="id_ch" value="${chip.id}" />
          <input type="submit" name="delete" value="Присвоить"/>
        </form>
      </td>
    </tr>
    </c:forEach>
    <form method="get" action="<c:url value='/'/>">
      <input type="submit" name="update" value="В главное меню"/>
    </form>
    </tbody>
  </table>
</div>
</body>
</html>