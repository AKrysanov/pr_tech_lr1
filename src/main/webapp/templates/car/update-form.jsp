<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="ru" xmlns:th="http://www.thymeleaf.org">

<head>
    <meta charset="UTF-8">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/css/bootstrap.min.css" integrity="sha384-DhY6onE6f3zzKbjUPRc2hOzGAdEf4/Dz+WJwBvEYL/lkkIsI3ihufq9hk9K4lVoK" crossorigin="anonymous">

    <title>Обновить информацию о машине</title>
</head>

<body>
<div class="container">
    <h3>Машины</h3>
    <hr>

    <p class="h4 mb-4">Обновить информацию о машине</p>

    <form method="post" action="<c:url value='/car/save'/>">

        <input type="number" hidden name="id" value="${car.id}" />

        <input type="text" name="name"/>

        <hr>

        <input type="submit" name="save" value="Сохранить"/>
    </form>
    <hr>
    <form method="get" action="<c:url value='/car/list'/>">
        <input type="submit" value="К списку машин"/>
    </form>
</div>
</body>

</html>