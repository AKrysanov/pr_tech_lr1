<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<head>
  <meta charset="UTF-8">

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/css/bootstrap.min.css" integrity="sha384-DhY6onE6f3zzKbjUPRc2hOzGAdEf4/Dz+WJwBvEYL/lkkIsI3ihufq9hk9K4lVoK" crossorigin="anonymous">

  <title>Список цветов</title>
</head>
<body>
<div class="container">

  <h3> <c:out value="${car.name}"/></h3>
  <hr>

  <table class="table table-bordered table-striped">
    <thead class="table-dark">
    <tr>
      <th>ID</th>
      <th>Цвет</th>
      <th>Действия</th>


    </tr>
    </thead>

    <tbody>
    <c:forEach var="tempColor" items="${color}">
      <tr>
      <td><c:out value="${tempColor.id}"/></td>
      <td><c:out value="${tempColor.name}"/></td>
      <td>
        <form method="post" action="<c:url value='/car/setcolor'/>">
          <input type="number" hidden name="id" value="${car.id}" />
          <input type="number" hidden name="id_col" value="${tempColor.id}" />
          <input type="submit" name="setcol" value="Прикрепить цвет"/>
        </form>
      </td>
    </tr>
    </c:forEach>

    </tbody>
  </table>


  <form method="get" action="<c:url value='/car/list'/>">
    <input type="submit" value="К списку машин"/>
  </form>


</div>
</body>
</html>