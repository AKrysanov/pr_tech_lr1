package com.akrysanov.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Chip {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH
            })
    @JoinTable(name = "tuning",
            joinColumns = { @JoinColumn(name = "chip_fk", referencedColumnName = "id")},
            inverseJoinColumns = { @JoinColumn(name = "engine_fk", referencedColumnName = "id")})
    private Set<Engine> Engines = new HashSet<>();

    public Chip(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Engine> getEngines() {
        return Engines;
    }

    public void setEngines(Set<Engine> Engines) {
        this.Engines = Engines;
    }

    @PreRemove
    private void preRemove() {
        for (Engine s : Engines){
            s.getChips().remove(this);
        }
    }
}
