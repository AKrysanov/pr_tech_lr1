package com.akrysanov.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Engine {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "hp")
    private String hp;

    @Column(name = "vol")
    private Float vol;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "Engines",
            cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH
            })
    private Set<Chip> Chips=new HashSet<>();

    public Engine(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHp() {
        return hp;
    }

    public void setHp(String hp) { this.hp = hp; }

    public Float getVol() {
        return vol;
    }

    public void setVol(Float vol) { this.vol = vol; }

    public Set<Chip> getChips() { return Chips; }

    public void setChips(Set<Chip> chips) { Chips = chips; }

    public boolean AddChip(Chip chip) { return Chips.add(chip); }

}
