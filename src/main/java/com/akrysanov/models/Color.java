package com.akrysanov.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Color {
    @Id
    @Column(name = "id_col")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name_col")
    private String name;

    @OneToMany(fetch = FetchType.LAZY ,mappedBy = "color",
            cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH
            })
    private List<Car> Cars = new ArrayList<>();

    public Color() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String Name) {
        this.name = Name;
    }

    public List<Car> getCars() {
        return Cars;
    }

    public void setCars(List<Car> Cars) {
        this.Cars = Cars;
    }



   @PreRemove
    private void preRemove() {
        for (Car s : Cars) {
            s.setColor(null);
        }
    }
}
