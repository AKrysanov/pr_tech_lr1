package com.akrysanov.servlets.EngineServlets;

import com.akrysanov.db_funcs.Chip_funcs;
import com.akrysanov.db_funcs.Engine_funcs;
import com.akrysanov.models.Chip;
import com.akrysanov.models.Engine;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class EngineAddChip extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Engine engine = Engine_funcs.findById(Integer.valueOf(request.getParameter("id_en")), true);
        engine.getChips().add(Chip_funcs.findById(Integer.valueOf(request.getParameter("id_ch")), false));
        Engine_funcs.save(engine);
        response.sendRedirect(request.getContextPath() + "/engine/list");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
