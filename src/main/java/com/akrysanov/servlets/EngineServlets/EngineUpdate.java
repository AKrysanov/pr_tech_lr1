package com.akrysanov.servlets.EngineServlets;

import com.akrysanov.db_funcs.Engine_funcs;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class EngineUpdate extends HttpServlet {
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("upd dopost " + req.getParameter("id"));
        req.setAttribute("engine", Engine_funcs.findById(Integer.valueOf(req.getParameter("id")), true));
        req.getRequestDispatcher("/templates/engine/update-form.jsp").forward(req, resp);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
