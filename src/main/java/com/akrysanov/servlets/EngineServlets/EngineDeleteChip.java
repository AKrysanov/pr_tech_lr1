package com.akrysanov.servlets.EngineServlets;

import com.akrysanov.db_funcs.Chip_funcs;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class EngineDeleteChip extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Chip_funcs.deleteChipRelationship(Integer.valueOf(request.getParameter("id_ch")), Integer.valueOf(request.getParameter("id_en")) );
        response.sendRedirect(request.getContextPath() + "/engine/list");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
