package com.akrysanov.servlets.EngineServlets;

import com.akrysanov.db_funcs.Engine_funcs;
import com.akrysanov.models.Engine;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class EngineSave extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Engine engine = new Engine();
        System.out.println(request.getParameter("id") + "    " + request.getParameter("hp") + request.getParameter("vol"));
        if(request.getParameter("id")!= "")
            engine.setId(Integer.valueOf(request.getParameter("id")));
        engine.setHp(request.getParameter("hp"));
        engine.setVol(Float.valueOf(request.getParameter("vol")));
        Engine_funcs.save(engine);
        //request.getRequestDispatcher("/templates/engine/show-all-engine.jsp").forward(request, response);
        response.sendRedirect(request.getContextPath() + "/engine/list");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
