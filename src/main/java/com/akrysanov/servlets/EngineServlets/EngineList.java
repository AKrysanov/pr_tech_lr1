package com.akrysanov.servlets.EngineServlets;

import com.akrysanov.db_funcs.Car_funcs;
import com.akrysanov.db_funcs.Engine_funcs;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class EngineList extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        req.setAttribute("engine", Engine_funcs.findAll());
        req.getRequestDispatcher("/templates/engine/show-all-engine.jsp").forward(req, resp);
    }
}
