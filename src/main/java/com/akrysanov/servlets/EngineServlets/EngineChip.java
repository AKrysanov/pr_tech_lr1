package com.akrysanov.servlets.EngineServlets;

import com.akrysanov.db_funcs.Chip_funcs;
import com.akrysanov.db_funcs.Engine_funcs;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class EngineChip extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("engine", Engine_funcs.findById(Integer.valueOf(request.getParameter("id")), true));
        request.getRequestDispatcher("/templates/engine/all-chip.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
