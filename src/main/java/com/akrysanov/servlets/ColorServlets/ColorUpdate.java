package com.akrysanov.servlets.ColorServlets;

import com.akrysanov.db_funcs.Color_funcs;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ColorUpdate extends HttpServlet {
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        System.out.println("upd dopost " + req.getParameter("id"));
        req.setAttribute("color", Color_funcs.findById(Integer.valueOf(req.getParameter("id"))));
        req.getRequestDispatcher("/templates/color/update-form.jsp").forward(req, resp);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
