package com.akrysanov.servlets.ColorServlets;

import com.akrysanov.db_funcs.Car_funcs;
import com.akrysanov.db_funcs.Color_funcs;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ColorList extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        req.setAttribute("color", Color_funcs.findAll());
        req.getRequestDispatcher("/templates/color/show-all-color.jsp").forward(req, resp);
    }
}
