package com.akrysanov.servlets.ColorServlets;

import com.akrysanov.db_funcs.Color_funcs;
import com.akrysanov.models.Color;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ColorSave extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        Color color = new Color();
        System.out.println(request.getParameter("id") + "    " + request.getParameter("name"));
        if(request.getParameter("id")!= "")
            color.setId(Integer.valueOf(request.getParameter("id")));
        color.setName(request.getParameter("name"));
        Color_funcs.save(color);
        //request.getRequestDispatcher("/templates/color/show-all-color.jsp").forward(request, response);
        response.sendRedirect(request.getContextPath() + "/color/list");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
