package com.akrysanov.servlets.CarServlets;

import com.akrysanov.db_funcs.Car_funcs;
import com.akrysanov.db_funcs.Color_funcs;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CarColor extends HttpServlet {
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("car", Car_funcs.findById(Integer.valueOf(req.getParameter("id"))));
        req.setAttribute("color", Color_funcs.findAll());
        System.out.println("carcolor "+Integer.valueOf(req.getParameter("id")));
        req.getRequestDispatcher("/templates/car/all-color.jsp").forward(req, resp);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
