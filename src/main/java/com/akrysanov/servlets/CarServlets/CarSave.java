package com.akrysanov.servlets.CarServlets;

import com.akrysanov.db_funcs.Car_funcs;
import com.akrysanov.models.Car;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CarSave extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        Car car = new Car();
        System.out.println(request.getParameter("id") + "    " + request.getParameter("name"));
        if(request.getParameter("id")!= "")
            car.setId(Integer.valueOf(request.getParameter("id")));
        car.setName(request.getParameter("name"));
        Car_funcs.save(car);
        //request.getRequestDispatcher("/templates/car/show-all-car.jsp").forward(request, response);
        response.sendRedirect(request.getContextPath() + "/car/list");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
