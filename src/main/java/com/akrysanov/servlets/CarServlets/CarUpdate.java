package com.akrysanov.servlets.CarServlets;

import com.akrysanov.db_funcs.Car_funcs;
import com.akrysanov.models.Car;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CarUpdate extends HttpServlet {
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        System.out.println("upd dopost " + req.getParameter("id"));
        req.setAttribute("car", Car_funcs.findById(Integer.valueOf(req.getParameter("id"))));
        req.getRequestDispatcher("/templates/car/update-form.jsp").forward(req, resp);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
