package com.akrysanov.servlets.CarServlets;

import com.akrysanov.db_funcs.Car_funcs;
import com.akrysanov.db_funcs.Color_funcs;
import com.akrysanov.models.Car;
import com.akrysanov.models.Color;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CarSetColor extends HttpServlet {
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("carsetcolor "+Integer.valueOf(req.getParameter("id")) + Integer.valueOf(req.getParameter("id_col")));
        Car car = Car_funcs.findById(Integer.valueOf(req.getParameter("id")));
        Color color = Color_funcs.findById(Integer.valueOf(req.getParameter("id_col")));
        car.setColor(color);
        color.getCars().add(car);
        Car_funcs.save(car);
        Color_funcs.save(color);
        resp.sendRedirect(req.getContextPath() + "/car/list");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
