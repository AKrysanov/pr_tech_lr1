package com.akrysanov.servlets.CarServlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.io.IOException;
import com.akrysanov.db_funcs.*;

public class CarList extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        req.setAttribute("car", Car_funcs.findAll());
        req.getRequestDispatcher("/templates/car/show-all-car.jsp").forward(req, resp);
    }
}
