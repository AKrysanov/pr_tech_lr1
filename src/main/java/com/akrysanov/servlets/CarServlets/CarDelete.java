package com.akrysanov.servlets.CarServlets;

import com.akrysanov.db_funcs.Car_funcs;
import com.akrysanov.models.Car;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CarDelete extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println(request.getParameter("id"));
        Car_funcs.deleteById(Integer.valueOf(request.getParameter("id")));
        response.sendRedirect(request.getContextPath() + "/car/list");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
