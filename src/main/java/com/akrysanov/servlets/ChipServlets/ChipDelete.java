package com.akrysanov.servlets.ChipServlets;

import com.akrysanov.db_funcs.Car_funcs;
import com.akrysanov.db_funcs.Chip_funcs;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ChipDelete extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println(request.getParameter("id"));
        Chip_funcs.deleteById(Integer.valueOf(request.getParameter("id")));
        response.sendRedirect(request.getContextPath() + "/chip/list");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
