package com.akrysanov.servlets.ChipServlets;

import com.akrysanov.db_funcs.Chip_funcs;
import com.akrysanov.db_funcs.Engine_funcs;
import com.akrysanov.models.Chip;
import com.akrysanov.models.Engine;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Set;

public class ChipSelectEngine extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("chip", Chip_funcs.findById(Integer.valueOf(request.getParameter("id_ch")), true));
        request.setAttribute("engine", Engine_funcs.findAll());
        request.getRequestDispatcher("/templates/chip/add-engine-to-chip.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
