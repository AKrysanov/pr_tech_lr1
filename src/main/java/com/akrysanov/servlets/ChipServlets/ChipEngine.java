package com.akrysanov.servlets.ChipServlets;

import com.akrysanov.db_funcs.Chip_funcs;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ChipEngine extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("chip", Chip_funcs.findById(Integer.valueOf(request.getParameter("id")), true));
        request.getRequestDispatcher("/templates/chip/all-engine.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
