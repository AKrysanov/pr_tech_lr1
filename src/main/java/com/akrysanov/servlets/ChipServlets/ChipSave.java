package com.akrysanov.servlets.ChipServlets;

import com.akrysanov.db_funcs.Chip_funcs;
import com.akrysanov.models.Chip;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ChipSave extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Chip chip = new Chip();
        System.out.println(request.getParameter("id") + "    " + request.getParameter("name"));
        if(request.getParameter("id")!= "")
            chip.setId(Integer.valueOf(request.getParameter("id")));
        chip.setName(request.getParameter("name"));
        Chip_funcs.save(chip);
        //request.getRequestDispatcher("/templates/chip/show-all-chip.jsp").forward(request, response);
        response.sendRedirect(request.getContextPath() + "/chip/list");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
