package com.akrysanov.servlets.ChipServlets;

import com.akrysanov.models.Chip;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ChipAdd extends HttpServlet {
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Chip chip =  new Chip();
        chip.setId(null);
        req.setAttribute("chip", chip);
        req.getRequestDispatcher("/templates/chip/update-form.jsp").forward(req, resp);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
