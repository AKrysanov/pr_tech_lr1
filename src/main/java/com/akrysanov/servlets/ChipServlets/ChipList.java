package com.akrysanov.servlets.ChipServlets;

import com.akrysanov.db_funcs.Car_funcs;
import com.akrysanov.db_funcs.Chip_funcs;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ChipList extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        req.setAttribute("chip", Chip_funcs.findAll());
        req.getRequestDispatcher("/templates/chip/show-all-chip.jsp").forward(req, resp);
    }
}
