package com.akrysanov.servlets.ChipServlets;

import com.akrysanov.db_funcs.Chip_funcs;
import com.akrysanov.db_funcs.Engine_funcs;
import com.akrysanov.models.Chip;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ChipAddEngine extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Chip chip = Chip_funcs.findById(Integer.valueOf(request.getParameter("id_ch")), true);
        chip.getEngines().add(Engine_funcs.findById(Integer.valueOf(request.getParameter("id_en")), false));
        Chip_funcs.save(chip);
        response.sendRedirect(request.getContextPath() + "/chip/list");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
