package com.akrysanov.servlets.ChipServlets;

import com.akrysanov.db_funcs.Chip_funcs;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ChipUpdate extends HttpServlet {
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("upd dopost " + req.getParameter("id"));
        req.setAttribute("chip", Chip_funcs.findById(Integer.valueOf(req.getParameter("id")), true));
        req.getRequestDispatcher("/templates/chip/update-form.jsp").forward(req, resp);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
