package com.akrysanov.db_funcs;

import com.akrysanov.models.*;
import java.sql.*;
import java.util.*;

public class Engine_funcs
{
    static String sql = "";
    private static String url = "jdbc:mysql://localhost:3306/car_properties?useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL = false";
    private static String username = "root";
    private static String password = "1234qwer";

    private static Engine mapEngine(ResultSet rs) throws SQLException {
        Engine engine = new Engine();

        engine.setId(rs.getInt("id"));
        engine.setHp(rs.getString("hp"));
        engine.setVol(rs.getFloat("vol"));

        return engine;
    }

    private static Chip mapChip(ResultSet rs) throws SQLException {
        Chip chip = new Chip();

        chip.setId(rs.getInt("id"));
        chip.setName(rs.getString("name"));

        return chip;
    }

    public static Iterable<Engine> findAll()
    {
        sql = "SELECT engine.id, engine.hp, engine.vol from engine";
        List<Engine> engineList = new ArrayList<>();
        Engine engine = null;
        try{
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
            try (Connection conn = DriverManager.getConnection(url, username, password)){
                Statement statement = conn.createStatement();
                ResultSet rs = statement.executeQuery(sql);
                while (rs.next()) {
                    int id = rs.getInt("id");

                    if (engine == null) {
                        engine = mapEngine(rs);
                    } else if (id != engine.getId()) {
                        engineList.add(engine);
                        engine = mapEngine(rs);
                    }
                }
                if (engine != null) {
                    engineList.add(engine);
                }
            }
        }
        catch(Exception ex){
            System.out.println(ex);
        }
        return engineList;
    }

    public static Engine findById(int id, boolean f)
    {
        sql = String.format(
                "SELECT engine.id, engine.hp, engine.vol," +
                        "chip.id FROM engine " +
                        "LEFT JOIN tuning on tuning.engine_fk = engine.id " +
                        "LEFT JOIN chip on tuning.chip_fk = chip.id " +
                        "WHERE engine.id = '%d' " + " union SELECT engine.id, engine.hp, engine.vol," +
                        "chip.id FROM engine " +
                        "Right JOIN tuning on tuning.engine_fk = engine.id " +
                        "Right JOIN chip on tuning.chip_fk = chip.id " +
                        "WHERE engine.id = '%d' ", id,id);
        Engine engine = null;
        try{
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
            try (Connection conn = DriverManager.getConnection(url, username, password)){

                Statement statement = conn.createStatement();
                ResultSet rs = statement.executeQuery(sql);
                while (rs.next()) {
                    if(engine==null || engine.getId()!=rs.getInt(1))
                        engine = mapEngine(rs);
                    if(f && rs.getString(4)!="")
                        engine.getChips().add(Chip_funcs.findById(rs.getInt(4), false));
                }
            }
        }
        catch(Exception ex){
            System.out.println(ex);
        }
        return engine;
    }

    public static void deleteById(int id)
    {
        try{
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
            deleteAllEngineRelationships(id);
            try (Connection conn = DriverManager.getConnection(url, username, password)){
                sql = "DELETE FROM engine WHERE id = ?";
                try(PreparedStatement preparedStatement = conn.prepareStatement(sql)){
                    preparedStatement.setInt(1, id);

                    preparedStatement.executeUpdate();
                }
            }
        }
        catch(Exception ex){
            System.out.println(ex);
        }
    }

    public static void save(Engine i) {

        Integer id = i.getId();

        try{
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
            try (Connection conn = DriverManager.getConnection(url, username, password))
            {
                if(id == null)
                {
                    sql = "INSERT INTO engine (hp, vol) values (?,?)";
                    try(PreparedStatement preparedStatement = conn.prepareStatement(sql)){
                        preparedStatement.setString(1, i.getHp());
                        preparedStatement.setFloat(2, i.getVol());
                        preparedStatement.executeUpdate();
                    }
                }
                else {
                    sql = "UPDATE engine SET hp = ?, vol = ? WHERE (id = ?)";
                    try(PreparedStatement preparedStatement = conn.prepareStatement(sql)){
                        preparedStatement.setString(1, i.getHp());
                        preparedStatement.setFloat(2, i.getVol());
                        preparedStatement.setInt(3, i.getId());
                        preparedStatement.executeUpdate();
                    }
                }
                Set<Chip> chipSet= i.getChips();
                if (i.getId()!= null){
                    String insertSql = "INSERT INTO tuning (`chip_fk`, `engine_fk`) VALUES (?, ?) ON DUPLICATE KEY UPDATE chip_fk = id";
                    deleteAllEngineRelationships(id);
                    for (Chip e: chipSet) {
                        try(PreparedStatement preparedStatement = conn.prepareStatement(insertSql)){
                            preparedStatement.setInt(1, e.getId());
                            preparedStatement.setInt(2, id);
                            preparedStatement.executeUpdate();
                        }
                    }
                }
            }
        }
        catch(Exception ex){
            System.out.println(ex);
        }



    }

    private static void deleteAllEngineRelationships(Integer id)
    {
        try{
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
            try (Connection conn = DriverManager.getConnection(url, username, password)){

                sql = "DELETE FROM tuning WHERE engine_fk=" + id;
                try(PreparedStatement preparedStatement = conn.prepareStatement(sql))
                {
                    preparedStatement.executeUpdate();
                }
            }
        }
        catch(Exception ex){
            System.out.println(ex);
        }
    }
}
