package com.akrysanov.db_funcs;

import com.akrysanov.models.*;
import java.sql.*;
import java.util.*;

public class Car_funcs
{
    static String sql = "";
    private static String url = "jdbc:mysql://localhost:3306/car_properties?useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL = false";
    private static String username = "root";
    private static String password = "1234qwer";

    private static Car mapCar(ResultSet rs)throws SQLException {
        Car car = new Car();
        car.setId(rs.getInt("id_car"));
        car.setName(rs.getString("name_car"));
        return car;
    }

    private static Color mapColor(ResultSet rs)throws SQLException {
        Color color = new Color();
        color.setId(rs.getInt("id_col"));
        color.setName(rs.getString("name_col"));
        return color;
    }

    public static Iterable<Car> findAll()
    {
        sql = "select * from car LEFT JOIN color on color.id_col = car.id_col";
        List<Car> carList = new ArrayList<>();
        Car car = null;
        try{
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
            try (Connection conn = DriverManager.getConnection(url, username, password)){
                Statement statement = conn.createStatement();
                ResultSet rs = statement.executeQuery(sql);
                while (rs.next()) {
                    int id = rs.getInt("id_car");

                    if (car == null) {
                        car = mapCar(rs);
                    }
                    else if (id != car.getId()) {
                        carList.add(car);
                        car = mapCar(rs);
                    }
                    //System.out.println(rs.getString("name_car"));
                    if (rs.getString("id_col") != null) {
                        Color color = mapColor(rs);
                        car.setColor(color);
                    }
                }
                if (car != null) {
                    carList.add(car);
                }
            }
        }
        catch(Exception ex){
            System.out.println(ex);
        }
        return carList;
    }

    public static Car findById(int id)
    {
        sql = String.format(
                "select * from car bi \n" +
                        "left join color co on co.id_col = bi.id_col \n"+
                        "where id_car = %d", id);
        Car car = new Car();
        try{
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
            try (Connection conn = DriverManager.getConnection(url, username, password)){

                Statement statement = conn.createStatement();
                ResultSet rs = statement.executeQuery(sql);
                while (rs.next()) {
                    car = mapCar(rs);
                    if (rs.getString("id_col") != null) {
                        Color color = mapColor(rs);
                        car.setColor(color);
                    }
                }
            }
        }
        catch(Exception ex){
            System.out.println(ex);
        }
        return car;
    }

    public static void deleteById(int id)
    {
        System.out.println("deletebyid");
        try{
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
            try (Connection conn = DriverManager.getConnection(url, username, password)){
                System.out.println("connectdb");
                sql = "DELETE FROM car WHERE id_car = ?";
                try(PreparedStatement preparedStatement = conn.prepareStatement(sql)){
                    preparedStatement.setInt(1, id);

                    preparedStatement.executeUpdate();
                }
            }
        }
        catch(Exception ex){
            System.out.println(ex);
        }
    }

    public static void save(Car i) {

        Integer id = i.getId();

        try{
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
            try (Connection conn = DriverManager.getConnection(url, username, password))
            {
                if(id == null)
                {
                    sql = "INSERT INTO car (name_car) values (?)";
                    try(PreparedStatement preparedStatement = conn.prepareStatement(sql)){
                        preparedStatement.setString(1, i.getName());
                        preparedStatement.executeUpdate();
                    }
                }
                else {
                    sql = "UPDATE car SET name_car = ? WHERE id_car = ?";
                    try(PreparedStatement preparedStatement = conn.prepareStatement(sql)){
                        preparedStatement.setString(1, i.getName());
                        preparedStatement.setInt(2, i.getId());
                        preparedStatement.executeUpdate();
                    }
                }
                if (i.getColor()!= null)
                {
                    sql = "UPDATE car SET id_col = ? WHERE id_car = ?";
                    try(PreparedStatement preparedStatement = conn.prepareStatement(sql)){
                        preparedStatement.setInt(1, i.getColor().getId());
                        preparedStatement.setInt(2, i.getId());
                        preparedStatement.executeUpdate();
                    }
                }
            }
        }
        catch(Exception ex){
            System.out.println(ex);
        }

    }
}
