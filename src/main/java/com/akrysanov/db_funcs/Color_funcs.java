package com.akrysanov.db_funcs;

import com.akrysanov.models.*;
import java.sql.*;
import java.util.*;

public class Color_funcs
{
    static String sql = "";
    private static String url = "jdbc:mysql://localhost:3306/car_properties?useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL = false";
    private static String username = "root";
    private static String password = "1234qwer";

     private static Color mapColor(ResultSet rs) throws SQLException{
        Color color = new Color();
        System.out.println(rs.getInt("id_col"));
        color.setId(rs.getInt("id_col"));
        color.setName(rs.getString("name_col"));
        return color;
    }

    public static Iterable<Color> findAll()
    {
        sql = "SELECT * FROM color";
        List<Color> colorList = new ArrayList<>();
        Color color = null;
        try{
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
            try (Connection conn = DriverManager.getConnection(url, username, password)){
                Statement statement = conn.createStatement();
                ResultSet rs = statement.executeQuery(sql);
                while (rs.next()) {
                    int id = rs.getInt("id_col");

                    if (color == null) {
                        color = mapColor(rs);
                    } else if (id != color.getId()) {
                        colorList.add(color);
                        color = mapColor(rs);
                    }
                }
                if (color != null) {
                    colorList.add(color);
                }
            }
        }
        catch(Exception ex){
            System.out.println(ex);
        }
        return colorList;
    }

    public static Color findById(int id)
    {
        sql = String.format(
                "SELECT color.id_col, color.name_col, car.id_car FROM color left join car on color.id_col = car.id_col   WHERE color.id_col = %d " +
                        "union SELECT color.id_col, color.name_col, car.id_car FROM color right join car on color.id_col = car.id_col   WHERE color.id_col = %d", id, id);
        Color color = null;
        try{
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
            try (Connection conn = DriverManager.getConnection(url, username, password)){

                Statement statement = conn.createStatement();
                ResultSet rs = statement.executeQuery(sql);
                while (rs.next()) {
                    if(color == null ||color.getId()!= rs.getInt("id_col"))
                        color = mapColor(rs);
                    if(rs.getString("id_car")!="") //System.out.println("car find");
                    color.getCars().add(Car_funcs.findById(rs.getInt(3)));
                }
            }
        }
        catch(Exception ex){
            System.out.println(ex);
        }
        return color;
    }

    public static void deleteById(int id)
    {
        try{
            if(!(Color_funcs.findById(id).getCars().isEmpty()))
            {
                System.out.println(findById(id).getCars().get(0).getId() + " dgbtrgtrg");
                for(Car c : findById(id).getCars())
                    Car_funcs.deleteById(c.getId());
            }
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
            try (Connection conn = DriverManager.getConnection(url, username, password)){
                System.out.println(id + " dbi");


                String sql = "DELETE FROM color WHERE id_col = ?";
                try(PreparedStatement preparedStatement = conn.prepareStatement(sql)){
                    System.out.println(id + " exec");
                    preparedStatement.setInt(1, id);

                    preparedStatement.executeUpdate();
                }
            }
        }
        catch(Exception ex){
            System.out.println(ex);
        }
    }

    public static void save(Color i) {

        Integer id = i.getId();

        try{
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
            try (Connection conn = DriverManager.getConnection(url, username, password))
            {
                if(id == null)
                {
                    sql = "INSERT INTO color (name_col) values (?)";
                    try(PreparedStatement preparedStatement = conn.prepareStatement(sql)){
                        preparedStatement.setString(1, i.getName());
                        preparedStatement.executeUpdate();
                    }
                }
                else {
                    sql = "UPDATE color SET name_col = ? WHERE id_col = ?";
                    try(PreparedStatement preparedStatement = conn.prepareStatement(sql)){
                        preparedStatement.setString(1, i.getName());
                        preparedStatement.setInt(2, i.getId());
                        preparedStatement.executeUpdate();
                    }
                }
            }
        }
        catch(Exception ex){
            System.out.println(ex);
        }

    }
}
