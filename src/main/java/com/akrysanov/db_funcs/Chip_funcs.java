package com.akrysanov.db_funcs;

import com.akrysanov.models.*;
import java.sql.*;
import java.util.*;

public class Chip_funcs
{
    static String sql = "";
    private static String url = "jdbc:mysql://localhost:3306/car_properties?useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL = false";
    private static String username = "root";
    private static String password = "1234qwer";

    private static Engine mapEngine(ResultSet rs) throws SQLException {
        Engine engine = new Engine();

        engine.setId(rs.getInt("id"));
        engine.setHp(rs.getString("hp"));
        engine.setVol(rs.getFloat("vol"));

        return engine;
    }

    private static Chip mapChip(ResultSet rs) throws SQLException {
        Chip chip = new Chip();

        chip.setId(rs.getInt("id"));
        chip.setName(rs.getString("name"));

        return chip;
    }

    public static Iterable<Chip> findAll()
    {
        sql ="SELECT chip.id, chip.name from chip";
        List<Chip> chipList = new ArrayList<>();
        try{
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
            try (Connection conn = DriverManager.getConnection(url, username, password)){
                Statement statement = conn.createStatement();
                ResultSet rs = statement.executeQuery(sql);
                Chip chip = null;
                while (rs.next()) {
                    int id = rs.getInt("id");
                    //System.out.println("id = " + id);
                    if (chip == null) {
                        chip = mapChip(rs);
                    } else if (id != chip.getId()) {
                        chipList.add(chip);
                        chip = mapChip(rs);
                    }
                    System.out.println(rs.getString("name"));
                    //System.out.println(rs.getString("name"));
                }
                if (chip != null) {
                    chipList.add(chip);
                }

            }
        }
        catch(Exception ex) {
            System.out.println(ex);
        }
        return chipList;
    }

    public static Chip findById(int id, boolean f)
    {
        sql = String.format(
                "SELECT chip.id, chip.name,\n" +
                        "engine.id\n" +
                        "FROM chip\n" +
                        "LEFT JOIN tuning on tuning.chip_fk = chip.id\n" +
                        "Left JOIN engine on tuning.engine_fk = engine.id\n" +
                        "WHERE chip.id = '%d'\n"+
                        "union "+ "SELECT chip.id, chip.name,\n" +
                "engine.id\n" +
                        "FROM chip\n" +
                        "Right JOIN tuning on tuning.chip_fk = chip.id\n" +
                        "Right JOIN engine on tuning.engine_fk = engine.id\n" +
                        "WHERE chip.id = '%d'\n", id, id);
        Chip chip = null;
        try{
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
            try (Connection conn = DriverManager.getConnection(url, username, password)){

                Statement statement = conn.createStatement();
                ResultSet rs = statement.executeQuery(sql);
                while (rs.next()) {
                    if(chip==null || chip.getId()!=rs.getInt(1))
                        chip = mapChip(rs);
                    if(f && rs.getString(3)!="")
                        chip.getEngines().add(Engine_funcs.findById(rs.getInt(3), false));
                }
            }
        }
        catch(Exception ex){
            System.out.println(ex);
        }
        return chip;
    }

    public static void deleteById(int id)
    {
        try{
            deleteAllChipRelationships(id);
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
            try (Connection conn = DriverManager.getConnection(url, username, password)){

                sql = "DELETE FROM chip WHERE id = ?";
                try(PreparedStatement preparedStatement = conn.prepareStatement(sql)){
                    preparedStatement.setInt(1, id);

                    preparedStatement.executeUpdate();
                }
            }
        }
        catch(Exception ex){
            System.out.println(ex);
        }
    }

    public static void save(Chip i) {

        Integer id = i.getId();

        try{
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
            try (Connection conn = DriverManager.getConnection(url, username, password))
            {
                if(id == null)
                {
                    sql = "INSERT INTO chip (name) values (?)";
                    try(PreparedStatement preparedStatement = conn.prepareStatement(sql)){
                        preparedStatement.setString(1, i.getName());
                        preparedStatement.executeUpdate();
                    }
                }
                else {
                    sql = "UPDATE chip SET name = ? WHERE id = ?";
                    try(PreparedStatement preparedStatement = conn.prepareStatement(sql)){
                        preparedStatement.setString(1, i.getName());
                        preparedStatement.setInt(2, i.getId());
                        preparedStatement.executeUpdate();
                    }
                }
                Set<Engine> engineSet= i.getEngines();
                if (i.getId()!= null){
                    String insertSql = "INSERT INTO tuning (`chip_fk`, `engine_fk`) VALUES (?, ?)";
                    deleteAllChipRelationships(id);
                    for (Engine e: engineSet) {
                        try(PreparedStatement preparedStatement = conn.prepareStatement(insertSql)){
                            preparedStatement.setInt(1, id);
                            preparedStatement.setInt(2, e.getId());
                            preparedStatement.executeUpdate();
                        }
                    }
                }

            }
        }
        catch(Exception ex){
            System.out.println(ex);
        }

    }

    private static void deleteAllChipRelationships(Integer id) {
        try{
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
            try (Connection conn = DriverManager.getConnection(url, username, password)){

                sql = "DELETE FROM tuning WHERE chip_fk=" + id;
                try(PreparedStatement preparedStatement = conn.prepareStatement(sql))
                {
                    preparedStatement.executeUpdate();
                }
            }
        }
        catch(Exception ex){
            System.out.println(ex);
        }
    }

    public static void deleteChipRelationship(Integer id_ch, int id_en) {
        try{
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
            try (Connection conn = DriverManager.getConnection(url, username, password)){

                sql = String.format("DELETE FROM tuning WHERE chip_fk = " + id_ch + " and engine_fk = "+ id_en);
                try(PreparedStatement preparedStatement = conn.prepareStatement(sql))
                {
                    preparedStatement.executeUpdate();
                }
            }
        }
        catch(Exception ex){
            System.out.println(ex);
        }
    }
}
